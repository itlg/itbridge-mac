#!/bin/bash

protoc $(pwd)/protos/itbridge.proto \
    --proto_path=$(pwd)/protos/ \
    --plugin=/usr/local/bin/protoc-gen-swift \
    --swift_opt=Visibility=Public \
    --swift_out=$(pwd)/itbridge/grpc/stubs \
    --plugin=/usr/local/bin/protoc-gen-grpc-swift \
    --grpc-swift_opt=Visibility=Public \
    --grpc-swift_out=$(pwd)/itbridge/grpc/stubs