import iTunesLibrary

class iTunesManager {
    var oItunes: ITLibrary?
    
    static let instance = iTunesManager()
    
    private init() {
        do {
            oItunes = try ITLibrary.init(apiVersion: "1.0")
        } catch {
            oItunes = nil
        }
    }
}
