//
//  main.swift
//  itbridge
//
//  Created by Gabriele Proietti Mattia on 03/05/2020.
//  Copyright © 2020 Gabriele Proietti Mattia. All rights reserved.
//

import Foundation
import iTunesLibrary
import GRPC
import NIO

/*
do {
    // let library = try ITLibrary.init(apiVersion: "1.0")
    // print(library.allMediaItems.count)
    
    // for item in library.allMediaItems {
    //    print(item.title)
    // }
    
} catch {
    print("error")
}
*/

/*
 * GRPC Server start
 */

let group = MultiThreadedEventLoopGroup(numberOfThreads: 1)
defer {
  try! group.syncShutdownGracefully()
}

// Start the server and print its address once it has started.
let server = Server.insecure(group: group)
  .withServiceProviders([iTbridgeProvider()])
  .bind(host: "localhost", port: 0)

server.map {
  $0.channel.localAddress
}.whenSuccess { address in
  print("server started on port \(address!.port!)")
}

// Wait on the server's `onClose` future to stop the program from exiting.
_ = try server.flatMap {
  $0.onClose
}.wait()
