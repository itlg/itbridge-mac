import GRPC
import NIO

class iTbridgeProvider: Itbridge_iTBridgeServiceProvider {
    func tracksStreamSelected(request: Itbridge_PayloadEmpty, context: StreamingResponseCallContext<Itbridge_TracksListStream>) -> EventLoopFuture<GRPCStatus> {
        
    }
    
    func tracksGetSelected(request: Itbridge_PayloadEmpty, context: StatusOnlyCallContext) -> EventLoopFuture<Itbridge_TracksList> {
        let iTunesApp = iTunesManager.instance.oItunes
        if(iTunesApp == nil) { return context.eventLoop.makeSucceededFuture(Itbridge_TracksList())  }
        return context.eventLoop.makeSucceededFuture(Itbridge_TracksList())
    }
    
    func trackSetLyrics(request: Itbridge_TrackIdAndLyrics, context: StatusOnlyCallContext) -> EventLoopFuture<Itbridge_PayloadOperationResult> {
        
    }
    
    func trackGetHasLyrics(request: Itbridge_TrackId, context: StatusOnlyCallContext) -> EventLoopFuture<Itbridge_PayloadBoolean> {
        
    }
    
    func hello(request: Itbridge_PayloadEmpty, context: StatusOnlyCallContext) -> EventLoopFuture<Itbridge_HelloReply> {
        let response = Itbridge_HelloReply.with {
            $0.appName = "itbdrige-mac";
            $0.appVersion = "0.0.1";
        }
        return context.eventLoop.makeSucceededFuture(response)
    }
}
